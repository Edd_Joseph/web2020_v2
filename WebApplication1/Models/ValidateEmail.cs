﻿using System;
using System.ComponentModel.DataAnnotations;
using WebApplication1.DAL;

namespace WebApplication1.Models
{
    public class ValidateEmail : ValidationAttribute
    {
        private CustomerDAL customerContext = new CustomerDAL();
        protected override ValidationResult IsValid(
        object value, ValidationContext validationContext)
        {
            // Get the email value to validate
            string email = Convert.ToString(value);
            // Casting the validation context to the "Customer" model class
            Customer customer = (Customer)validationContext.ObjectInstance;
            // Get the Customer Id from the customer instance
            int customerId = customer.CustomerID;
            if (customerContext.IsEmailExist(email, customerId))
                // validation failed
                return new ValidationResult
                ("Email address already exists!");
            else
                // validation passed
                return ValidationResult.Success;
        }

    }
}
