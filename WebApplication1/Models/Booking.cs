﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Booking
    {
        public int BookingID { get; set; }

        public int CustomerID { get; set; }

        [Display(Name = "ScheduleID")]
        public int ScheduleID { get; set; }

        [Required]
        [Display(Name = "Passenger Name")]
        public string PassengerName { get; set; }

        [Required]
        [Display(Name = "Passport Number")]
        public string PassortNumber { get; set; }

        [Required]
        public string Nationality { get; set; }

        [Required]
        [Display(Name = "Seat Class")]
        public string SeatClass { get; set; }

        [Required]
        [Display(Name = "Total Amount")]
        [DataType(DataType.Currency)]
        public decimal AmtPayable { get; set; }

        public string Remarks { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateTimeCreated { get; set; }
    }
}
