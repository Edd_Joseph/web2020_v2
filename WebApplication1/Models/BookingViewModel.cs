﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class BookingViewModel
    {
        public List<FlightScheduleViewModel> list { get; set; }

        public BookingViewModel()
        {
            list = new List<FlightScheduleViewModel>();
        }
    }
}
