﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }

        [Required, MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        [Display(Name = "Name")]
        public string CustomerName { get; set; }

        [MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        [Display(Name = "Country")]
        public string Nationality { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime BirthDate { get; set; }

        [MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        public string TelNo { get; set; }


        [Required, MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        [Display(Name = "Email Address")]
        [EmailAddress]
        [ValidateEmail]
        public string EmailAddr { get; set; }


        [MaxLength(255, ErrorMessage = "Please do not exceed 255 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


    }
}
