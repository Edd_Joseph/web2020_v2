﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class StaffViewModel
    {

        [Required]
        [Display(Name = "Staff ID")]
        public int StaffID { get; set; }


        [Required(ErrorMessage = "Please enter a name")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        [Display(Name = "Staff Name")]
        public string StaffName { get; set; }

        [Required]
        [Display(Name = "Gender")]
        [RegularExpression("^(m|M|F|f)", ErrorMessage = "Please enter M/F")]
        public char Gender { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        [Display(Name = "Vocation")]
        public string Vocation { get; set; }

        public string Role { get; set; }

        [Required]
        [Display(Name = "Schedule ID")]
        public int ScheduleID { get; set; }
    }
}
