using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class FlightRouteViewModel
    {
        [Display(Name = "Route ID")]
        public int RouteID { get; set; }

        [Display(Name = "Departure City")]
        [RegularExpression(@"^[a-zA-Z  ]+$", ErrorMessage = "City names only have letters!")]
        public string DepartureCity { get; set; }

        [Display(Name = "Departure Country")]
        [RegularExpression(@"^[a-zA-Z  ]+$", ErrorMessage = "Country name only have letters!")]
        public string DepartureCountry { get; set; }

        [Display(Name = "Arrival City")]
        [RegularExpression(@"^[a-zA-Z  ]+$", ErrorMessage = "City names only have letters!")]
        public string ArrivalCity { get; set; }

        [Display(Name = "Arrival Country")]
        [RegularExpression(@"^[a-zA-Z  ]+$", ErrorMessage = "Country names only have letters!")]
        public string ArrivalCountry { get; set; }

        [Display(Name = "Flight Duration")]
        public int FlightDuration { get; set; }
    }
}
