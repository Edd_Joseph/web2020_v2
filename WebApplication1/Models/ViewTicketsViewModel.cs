﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class ViewTicketsViewModel
    {

        public List<Booking> list { get; set; }

        public ViewTicketsViewModel()
        {
            list = new List<Booking>();
        }
    }
}
