﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class TicketDetailsViewModel
    {
        public List<Booking> bookinglist { get; set; }
        public List<FlightScheduleViewModel> flightlist { get; set; }
        public TicketDetailsViewModel()
        {
            bookinglist = new List<Booking>();
            flightlist = new List<FlightScheduleViewModel>();
        }
    }
}
