﻿using System;
using System.ComponentModel.DataAnnotations;


namespace WebApplication1.Models
{
    public class Staff
    {
        [Required]
        [Display(Name = "ID")]
        public int StaffID { get; set; }


        [Required(ErrorMessage = "Please enter a name")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        [Display(Name = "Staff Name")]
        public string StaffName { get; set; }

        [Required]
        [Display(Name = "Gender")]
        [RegularExpression("^(m|M|F|f)", ErrorMessage = "Please enter M/F")]
        public char Gender { get; set; }

        [Required]
        [DataType(DataType.Date)]
        //Check here
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DateEmployed { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        [Display(Name = "Vocation")]
        public string Vocation { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        [EmailAddress]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string EmailAddr { get; set; }


        [Required]
        [MaxLength(255, ErrorMessage = "Please do not exceed 255 characters")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        [Required]
        [Display(Name = "Status")]
        [MaxLength(50, ErrorMessage = "Please do not exceed 50 characters")]
        public string Status { get; set; }
    }
}
