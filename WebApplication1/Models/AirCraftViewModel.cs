﻿using System;
using System.ComponentModel.DataAnnotations;


namespace WebApplication1.Models
{
    public class AirCraftViewModel
    {
        public AirCraftViewModel()
        {
            Status = "Operational";
        }
        public int AirCraftId { get; set; }

        [Required]
        [Display(Name = "AirCraft Model")]
        public string Model { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Number of Economy seats should be in digits")]
        [Display(Name = "Number Of Economy Seats")]
        public int EconomySeats { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Number of Business seats should be in digits")]
        [Display(Name = "Number Of Business Seats")]
        public int BusinessSeats { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Last Maintenance")]
        public DateTime LastMaintenance { get; set; }


        [Required]
        [Display(Name = "Status")]
        public string Status { get; set; }




    }

}