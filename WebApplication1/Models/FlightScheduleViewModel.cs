﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class FlightScheduleViewModel
    {
        public int ScheduleId { get; set; }
        public int RouteId { get; set; }
        public int? AircraftId { get; set; }

        [Required]
        [Display(Name = "Flight Number")]
        public string FlightNumber { get; set; }

        [Required]
        [Display(Name = "Departure DateTime")]
        public DateTime? DepartureDateTime { get; set; }

        [Required]
        [Display(Name = "Arrival DateTime")]
        public DateTime? ArrivalDateTime { get; set; }

        [Required]
        [Display(Name = "Economy Class Price")]
        public decimal EconomyClassPrice { get; set; }

        [Required]
        [Display(Name = "Business Class Price")]
        public decimal BusinessClassPrice { get; set; }

        [Required]
        [Display(Name = "Status")]
        public string Status { get; set; }



    }
}