﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class AircraftScheduleViewModel
    {
        public int AirCraftId { get; set; }

        [Required]
        [Display(Name = "AirCraft Model")]
        public string Model { get; set; }
        public int ScheduleId { get; set; }
        public int RouteId { get; set; }

        [Required]
        [Display(Name = "Flight Number")]
        public string FlightNumber { get; set; }

        [Required]
        [Display(Name = "Departure DateTime")]
        public DateTime DepartureDateTime { get; set; }

        [Required]
        [Display(Name = "Arrival DateTime")]
        public DateTime ArrivalDateTime { get; set; }

        [Required]
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}
