﻿namespace WebApplication1.Models
{
    public class FlightCrew
    {
        public int ScheduleID { get; set; }

        public int StaffID { get; set; }

        public string Role { get; set; }

    }
}
