﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;

namespace WebApplication1.Controllers
{
    public class ViewTicketsController : Controller
    {
        private BookingDAL bookingContext = new BookingDAL();
        // GET: ViewTicketsController
        public ActionResult Index()
        {

            ViewTicketsViewModel book = new ViewTicketsViewModel();
            int id = Convert.ToInt32(HttpContext.Session.GetString("Id"));
            book.list = bookingContext.GetBookingDetailsByCustomerId(id);
            return View(book);
        }


         public ActionResult TicketDetails(int? id)
        {

           
            TicketDetailsViewModel ticket = new TicketDetailsViewModel();
            if (id != null)
            {
                ticket.bookinglist = bookingContext.GetBookingDetailsByBookingId(Convert.ToInt32(id));
                int id2 = bookingContext.GetScheduleId(id.Value);
                ticket.flightlist = bookingContext.GetFlightScheduleByScheduleId(id2);
            }
            
            return View(ticket);
        }

        // GET: ViewTicketsController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ViewTicketsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ViewTicketsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ViewTicketsController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ViewTicketsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ViewTicketsController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ViewTicketsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
