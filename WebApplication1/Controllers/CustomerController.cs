﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using WebApplication1.DAL;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class CustomerController : Controller
    {
        private CustomerDAL customerContext = new CustomerDAL();
        public ActionResult Index()
        {
            return View();
        }

        public IActionResult ChangePasswordCustomer()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ChangePasswordCustomer(IFormCollection formData)
        {
            
            if (ModelState.IsValid)
            {
                string password = formData["NewPassword"].ToString();
                string cpassword = formData["ConfirmPassword"].ToString();
                int id = Convert.ToInt32(HttpContext.Session.GetString("Id"));
                if (password == cpassword)
                {
                    customerContext.UpdatePassword(id, password);
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Passwordc"] = "Passwords do not match!";
                    return View();
                }
            }
            else
            {
                return View();
            }
        }

        


        public IActionResult BookTickets()
        {
            return View();  
        }

        public IActionResult ViewTickets()
        {
            return View();
        }

        // GET: Customer/Register
        public ActionResult Register()
        {
            return View();
        }

        // POST: Customer/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Customer customer, IFormCollection formData)
        {
            if (ModelState.IsValid)
            {
                //Add staff record to database
                customer.CustomerID = customerContext.Add(customer);
                string email = formData["EmailAddr"].ToString();
                string name = formData["CustomerName"].ToString();
                HttpContext.Session.SetString("Email", email);
                HttpContext.Session.SetString("Name", name);
                HttpContext.Session.SetString("Id", Convert.ToString(customerContext.GetId(email)));
                //Redirect user to Staff/Index view
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Message3"] = "Invalid Login Credentials!";
                //Input validation fails, return to the Create view
                //to display error message
                return View();
            }

        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string email = formData["EmailAddr"].ToString();
            string password = formData["Password"].ToString();

            if (customerContext.IsPasswordCorrect(email, password) == false)
            {

                TempData["Message"] = "Invalid Login Credentials!";
                return View();
            }
            else
            {
                string name = customerContext.GetName(email);
                HttpContext.Session.SetString("Name", name);
                HttpContext.Session.SetString("Email", email);
                HttpContext.Session.SetString("Id", Convert.ToString(customerContext.GetId(email)));
                return RedirectToAction("Index");
            }
        }
    }
}
