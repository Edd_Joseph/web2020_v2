﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication1.DAL;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AdminController : Controller
    {
        private AdminDAL adminContext = new AdminDAL();

        // GET: Project/Edit/5
        public ActionResult AddAirCraft()
        {

            return View();
        }

        // POST: Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAirCraft(AirCraftViewModel val)
        {


            if (ModelState.IsValid)
            {
                //Update staff record to database

                adminContext.AddAircraft(val);
                return RedirectToAction("ViewAirCrafts");


            }

            return RedirectToAction("AddAirCraft");
        }
        public IActionResult AircraftMaintenanceFilter()
        {
            List<AirCraftViewModel> detailslist = adminContext.GetAllAircraft();
            return View(detailslist);
        }
        public IActionResult ChangePassword()
        {
            return View("ChangePassword");
        }
        public IActionResult Index()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Staff" role

            List<AircraftScheduleViewModel> detailslist = adminContext.GetAllAircraftSchedule();
            return View(detailslist);
        }
       

        public ActionResult StaffLogIn()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        //This function is called when your submit button is clicked 
        public ActionResult Login(IFormCollection formData)
        {
            //validate pass and user name 
            String username = formData["EmailAddr"].ToString();
            String password = formData["Password"].ToString();
            // username is "abcd@lionnet.com" password is "p@ssw0rd" 
            // boolean expression 
            // (variable/number) ** (variable/number) --> boolean operator --> <= < > >= == != 
            // x == 5 
            // when you want combine 2 boolean expresion 
            // && || 

            if (username == "abcd@lionet.com" && password == "p@ssw0rd")
            {
                // then you set the session variable httpContext.Session.setString("Role") = ""'
                // redirect to staffHomePage 
                // This is to set the session variable with the key called role to be "Staff"
                HttpContext.Session.SetString("Role", "Staff");
                //Reason why redirect to index funciton is because index funciton will take care of redirecting to home or staff view page
                return Redirect("Index");
            }
            else
            {
                // if i enter the wrong password 
                TempData["message"] = "Wrong email / password";
            }
            //Delete mext 2 line when you submit your code 
            // uncomment the next line that returns View("...");
            //HttpContext.Session.SetString("Role", "Staff");
            //return Redirect("Index");
            return View("StaffLogIn");
        }


        public IActionResult UpdateStatus(string id)
        {
            adminContext.UpdateStatus(id);
            List<AirCraftViewModel> detailslist = adminContext.GetAllAircraft();
            return View("AircraftMaintenanceFilter",detailslist);
        }
        public IActionResult ViewAirCrafts()
        {
            List<AircraftScheduleViewModel> aircraft = adminContext.GetAllAircraftSchedule();
            return View(aircraft);
        }

        // FlightSchedule
        public ActionResult AddFlightSchedule()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddFlightSchedule(FlightScheduleViewModel val)
        {

            if (ModelState.IsValid)
            {
                //Update staff record to database

                adminContext.AddFlightSchedule(val);
                return RedirectToAction("ViewSchedule");


            }

            return RedirectToAction("AddFlightSchedule");
        }
        public IActionResult ViewSchedule()
        {
            List<FlightScheduleViewModel> sched = adminContext.ViewFlightSchedule();
            return View(sched);
        }
        public ActionResult UpdateFlightSchedule()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateFlightSchedule(FlightScheduleViewModel val)
        {

            if (ModelState.IsValid)
            {
                //Update staff record to database

                adminContext.UpdateFlightSchedule(val);
                return RedirectToAction("ViewSchedule");


            }

            return RedirectToAction("UpdateFlightSchedule");
        }

        // FlightRoutes
        public ActionResult AddFlightRoute()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddFlightRoute(FlightRouteViewModel val)
        {

            if (ModelState.IsValid)
            {
                //Update staff record to database
                adminContext.AddRoute(val);
                return RedirectToAction("ViewRoutes");
            }
            return RedirectToAction("AddFlightRoute");
        }
        public IActionResult ViewRoutes()
        {
            List<FlightRouteViewModel> routes = adminContext.ViewRoutes();
            return View(routes);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteFlightRoute(FlightRouteViewModel val)
        {

            if (ModelState.IsValid)
            {
                //Update staff record to database
                adminContext.DeleteRoute(val);
                return RedirectToAction("ViewRoutes");
            }
            return RedirectToAction("DeleteFlightRoute");
        }

        public ActionResult AssignPilot(int ScheduleID, int StaffID)
        {
            FlightCrew flightCrew = new FlightCrew
            {
                ScheduleID = ScheduleID,
                StaffID = StaffID
            };
            List<SelectListItem> Roles = new List<SelectListItem>();
            Roles.Add(new SelectListItem { Value = "Flight Captain", Text = "Flight Captain" });
            Roles.Add(new SelectListItem { Value = "Second Pilot", Text = "Second Pilot" });
            ViewData["Roles"] = Roles;
            return View(flightCrew);
        }

        
        /*[HttpPost]
        public ActionResult AssignPilot(FlightCrew flightCrew)
        {
            adminContext.checkAssignment(flightCrew.Role, flightCrew.ScheduleID);
            return View();
        } */
        
        
        public ActionResult CreatePersonnel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreatePersonnel(Staff staff)
        {
            if (ModelState.IsValid)
            {
                adminContext.AddStaff(staff);
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult ViewStaff()
        {
            List<StaffViewModel> staffList = adminContext.getAllStaffWithRoles();
            // go into database get all the staffs with their roles. 
            return View(staffList);
        }

        public ActionResult ViewFlightSchedule()
        {
            // How do i get the list of flight schedules 
            // i need to call staffDAL funciton (getAvaliableFlightSchedules)
            // in order to call a function from another file /class 
            // i need the object for that class. 
            // here the object is called staffContext

            List<FlightScheduleViewModel> FlightScheduleList = adminContext.getAvaliableFlightSchedules();
            return View(FlightScheduleList);
        }

        public ActionResult ViewAvailablePilots(int ScheduleID)
        {
            List<Staff> avaliablePliots = adminContext.GetAvaliablePilotStaff();
            TempData["ScheduleID"] = ScheduleID;
            return View(avaliablePliots);
        }
    }

}