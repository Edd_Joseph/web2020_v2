﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using System.Linq.Expressions;

namespace WebApplication1.Controllers
{
    public class BookingController : Controller
    {
        private BookingDAL bookingContext = new BookingDAL();
        // GET: BookingController
        public ActionResult Index()
        {
            BookingViewModel book = new BookingViewModel();
            book.list = bookingContext.GetFlightScheduleByStatus("Opened");

            return View(book);
        }

        public ActionResult ResultPage()
        {
            
            return View();
        }

        public ActionResult BookTickets(int? id)
        {
            
            if (id != null)
            {
                
                
                HttpContext.Session.SetString("ScheduleID", Convert.ToString(id));
                
                ViewData["ClassList"] = GetClass();
                
            }
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BookTickets(Booking booking, IFormCollection formData)
        {

            ViewData["ClassList"] = GetClass();

            if (ModelState.IsValid)
            {
                ViewData["ShowResult"] = true;
                decimal price = 0;
                //Add staff record to database
                int id = Convert.ToInt32(HttpContext.Session.GetString("Id"));
                int schedule = Convert.ToInt32(HttpContext.Session.GetString("ScheduleID"));
                string seatclass = formData["SeatClass"].ToString();
                HttpContext.Session.SetString("SeatClass", seatclass);
                List<decimal> pricelist = new List<decimal>();
                pricelist = bookingContext.GetPrice(schedule);
                if (seatclass == "Economy")
                {
                     price = pricelist[0];
                }
                else
                {
                     price = pricelist[1];
                }
                bookingContext.Add(booking, id, schedule, price);
                TempData["Price"] = Convert.ToString(price);
                TempData["Id"] = Convert.ToString(schedule);
                TempData["Seat"] = Convert.ToString(seatclass);
                
                return RedirectToAction("ResultPage");
            }
            else
            {
                return View();
            }
        }

        private List<SelectListItem> GetClass()
        {
            List<SelectListItem> classtier = new List<SelectListItem>();

            classtier.Add(new SelectListItem
            {
                Value = "Economy",
                Text = "Economy"
            });
            classtier.Add(new SelectListItem
            {
                Value ="Business",
                Text = "Business"
            });
           
            return classtier;
        }

        // GET: BookingController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BookingController/Create
        public ActionResult Booking()
        {
            return View();
        }

   

        // GET: BookingController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BookingController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: BookingController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BookingController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
