﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WebApplication1.Models;

namespace WebApplication1.DAL
{
    public class AdminDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public AdminDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            //Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "AirFlightsConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }


        public List<AircraftScheduleViewModel> GetAllAircraftSchedule()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT a.AircraftID, a.MakeModel, fs.ScheduleID, fs.FlightNumber,fs.RouteID, fs.DepartureDateTime, fs.ArrivalDateTime,fs.[Status] FROM FlightSchedule fs LEFT JOIN Aircraft a ON fs.AircraftID = a.AircraftID", conn);

            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "ScheduleDetails");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<AircraftScheduleViewModel> alldetailsList = new List<AircraftScheduleViewModel>();
            foreach (DataRow row in result.Tables["ScheduleDetails"].Rows)
            {
                alldetailsList.Add(
                new AircraftScheduleViewModel
                {
                    AirCraftId = Convert.ToInt32(row["AircraftID"]),
                    Model = row["MakeModel"].ToString(),
                    ScheduleId = Convert.ToInt32(row["ScheduleID"]),
                    FlightNumber = row["FlightNumber"].ToString(),
                    RouteId = Convert.ToInt32(row["RouteID"]),
                    DepartureDateTime = (DateTime)row["DepartureDateTime"],
                    ArrivalDateTime = (DateTime)row["ArrivalDateTime"],
                    Status = row["Status"].ToString()

                }

                );
            }
            return alldetailsList;
        }
        public List<AirCraftViewModel> GetAllAircraft()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Aircraft", conn);

            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "ScheduleDetails");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<AirCraftViewModel> alldetailsList = new List<AirCraftViewModel>();
            foreach (DataRow row in result.Tables["ScheduleDetails"].Rows)
            {
                alldetailsList.Add(
                new AirCraftViewModel
                {
                    AirCraftId = Convert.ToInt32(row["AircraftID"]),
                    Model = row["MakeModel"].ToString(),
                    EconomySeats = Convert.ToInt32(row["NumEconomySeat"]),
                    BusinessSeats = Convert.ToInt32(row["NumBusinessSeat"]),
                    LastMaintenance = (DateTime)row["DateLastMaintenance"],
                    Status = row["Status"].ToString()

                }

                );
            }
            return alldetailsList;
        }

        

        public int AddAircraft(AirCraftViewModel airCraftViewModel)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated StaffID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
            ("INSERT INTO Aircraft (MakeModel, NumEconomySeat, NumBusinessSeat, DateLastMaintenance) " +
            "OUTPUT INSERTED.AircraftID " +
            "VALUES(@model, @eco, @bus, @lm)", conn);
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            //cmd.Parameters.AddWithValue("@ID", airCraftViewModel.AirCraftId);
            cmd.Parameters.AddWithValue("@model", airCraftViewModel.Model);
            cmd.Parameters.AddWithValue("@eco", airCraftViewModel.EconomySeats);
            cmd.Parameters.AddWithValue("@bus", airCraftViewModel.BusinessSeats);
            cmd.Parameters.AddWithValue("@lm", airCraftViewModel.LastMaintenance);

            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //ProjectID after executing the INSERT SQL statement
            airCraftViewModel.AirCraftId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return airCraftViewModel.AirCraftId;
        }

        //Flight Schedule DAL
        public List<FlightScheduleViewModel> ViewFlightInfo(FlightScheduleViewModel flightdetails)
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM FlightSchedule Where ScheduleID=@SchedID", conn);


            cmd.Parameters.AddWithValue("@ScheduleID", flightdetails.ScheduleId);
            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "ScheduleDetails");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<FlightScheduleViewModel> alldetailsList = new List<FlightScheduleViewModel>();
            foreach (DataRow row in result.Tables["ScheduleDetails"].Rows)
            {
                alldetailsList.Add(
                new FlightScheduleViewModel
                {
                    ScheduleId = Convert.ToInt32(row["ScheduleID"]),
                    FlightNumber = row["FlightNumber"].ToString(),
                    AircraftId = Convert.ToInt32(row["AirCraftID"]),
                    RouteId = Convert.ToInt32(row["RouteID"]),
                    DepartureDateTime = (DateTime)row["DepartureDateTime"],
                    ArrivalDateTime = (DateTime)row["ArrivalDateTime"],
                    EconomyClassPrice = Convert.ToInt32(row["EconomyClassPrice"]),
                    BusinessClassPrice = Convert.ToInt32(row["BusinessClassPrice"]),
                    Status = row["Status"].ToString()

                }

                );
            }
            return alldetailsList;
        }
        public int UpdateFlightSchedule(FlightScheduleViewModel assignflight)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                 (" UPDATE FlightSchedule SET RouteID=@RouteID,AircraftID=@AirCraftID,DepartureDateTime=@DepDT,ArrivalDateTime=@ArrDT,EconomyClassPrice=@EconP,BusinessClassPrice=@BizP,Status=@Status   " +
                 " WHERE ScheduleID = @ScheduleID", conn);
            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from the respective property of “project” object. 
            cmd.Parameters.AddWithValue("@RouteID", assignflight.RouteId);
            cmd.Parameters.AddWithValue("@AircraftID", assignflight.AircraftId);
            cmd.Parameters.AddWithValue("@DepDT", assignflight.DepartureDateTime);
            cmd.Parameters.AddWithValue("@ArrDT", assignflight.ArrivalDateTime);
            cmd.Parameters.AddWithValue("@EconP", assignflight.EconomyClassPrice);
            cmd.Parameters.AddWithValue("@BizP", assignflight.BusinessClassPrice);
            cmd.Parameters.AddWithValue("@Status", assignflight.Status);
            cmd.Parameters.AddWithValue("@ScheduleID", assignflight.ScheduleId);

            //Open a database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection.
            conn.Close();
            return count;
        }

        public int AddFlightSchedule(FlightScheduleViewModel flightSchedule)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                 (" INSERT INTO FLIGHTSCHEDULE(FlightNumber,RouteID,AircraftID,DepartureDateTime,ArrivalDateTime,EconomyClassPrice,BusinessClassPrice,Status) " +
                 "OUTPUT INSERTED.ScheduleID " +
                 " VALUES(@FlightNo,@RouteID,@AircraftID,@DepDT,@ArrDT,@EconPrice,@BizPrice,@Status", conn);
            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from the respective property of “project” object. 
            cmd.Parameters.AddWithValue("@FlightNo", flightSchedule.FlightNumber);
            cmd.Parameters.AddWithValue("@RouteID", flightSchedule.RouteId);

            // Since we allow null value for AircraftId, need to check
            if (flightSchedule.AircraftId != null && flightSchedule.AircraftId != 0)
                cmd.Parameters.AddWithValue("@AircraftID",
                                                                       flightSchedule.AircraftId.Value);
            else
                cmd.Parameters.AddWithValue("@AircraftID", DBNull.Value);

            cmd.Parameters.AddWithValue("@DepDT", flightSchedule.DepartureDateTime);
            cmd.Parameters.AddWithValue("@ArrDT", flightSchedule.ArrivalDateTime);
            cmd.Parameters.AddWithValue("@EconPrice", flightSchedule.EconomyClassPrice);
            cmd.Parameters.AddWithValue("@BizPrice", flightSchedule.BusinessClassPrice);
            cmd.Parameters.AddWithValue("@Status", flightSchedule.Status);

            //Open a database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection.
            conn.Close();
            return count;
        }

        public List<FlightScheduleViewModel> ViewFlightSchedule()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM FlightSchedule", conn);

            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "ScheduleDetails");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<FlightScheduleViewModel> alldetailsList = new List<FlightScheduleViewModel>();
            foreach (DataRow row in result.Tables["ScheduleDetails"].Rows)
            {
                alldetailsList.Add(
                new FlightScheduleViewModel
                {
                    ScheduleId = Convert.ToInt32(row["ScheduleID"]),
                    FlightNumber = row["FlightNumber"].ToString(),
                    AircraftId = Convert.ToInt32(row["AirCraftID"]),
                    RouteId = Convert.ToInt32(row["RouteID"]),
                    DepartureDateTime = (DateTime)row["DepartureDateTime"],
                    ArrivalDateTime = (DateTime)row["ArrivalDateTime"],
                    EconomyClassPrice = Convert.ToInt32(row["EconomyClassPrice"]),
                    BusinessClassPrice = Convert.ToInt32(row["BusinessClassPrice"]),
                    Status = row["Status"].ToString()

                }

                );
            }
            return alldetailsList;
        }
        public int UpdateFlightScheduleStatus(FlightScheduleViewModel assignflight)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                 (" UPDATE FlightSchedule SET Status=@Status, " +
                  " WHERE ScheduleID = @ScheduleID", conn);
            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from the respective property of “project” object. 
            cmd.Parameters.AddWithValue("@Status", assignflight.Status);
            cmd.Parameters.AddWithValue("@ScheduleID", assignflight.ScheduleId);

            //Open a database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection.
            conn.Close();
            return count;
        }

        // Flight Route DAL
        public int AddRoute(FlightRouteViewModel flightRoute)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                 (" INSERT INTO FlightRoute (DepartureCity,DepartureCountry,ArrivalCity,ArrivalCountry,FlightDuration) " +
                  " OUTPUT INSERTED.RouteID" +
                  " VALUES(@DepCity,@DepCountry,@ArrCity,@ArrCountry,@Duration)", conn);
            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from the respective property of “project” object.
            cmd.Parameters.AddWithValue("@DepCity", flightRoute.DepartureCity);
            cmd.Parameters.AddWithValue("@DepCountry", flightRoute.DepartureCountry);
            cmd.Parameters.AddWithValue("@ArrCity", flightRoute.ArrivalCity);
            cmd.Parameters.AddWithValue("@ArrCountry", flightRoute.ArrivalCountry);
            cmd.Parameters.AddWithValue("@Duration", flightRoute.FlightDuration);

            //Open a database connection.
            conn.Open();

            int Route = cmd.ExecuteNonQuery();
            //Close the database connection.
            conn.Close();
            return Route;
        }

        public List<FlightRouteViewModel> ViewRoutes()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM FlightRoute", conn);

            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "FlightRoutes");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<FlightRouteViewModel> FlightRouteList = new List<FlightRouteViewModel>();
            foreach (DataRow row in result.Tables["FlightRoutes"].Rows)
            {
                FlightRouteList.Add(
                new FlightRouteViewModel
                {
                    RouteID = Convert.ToInt32(row["RouteID"]),
                    DepartureCity = row["DepartureCity"].ToString(),
                    DepartureCountry = row["DepartureCountry"].ToString(),
                    ArrivalCity = row["ArrivalCity"].ToString(),
                    ArrivalCountry = row["ArrivalCountry"].ToString(),
                    FlightDuration = Convert.ToInt32(row["FlightDuration"])

                }

                );
            }
            return FlightRouteList;
        }
        // Flight Route DAL
        public int DeleteRoute(FlightRouteViewModel flightRoute)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                 (" DELETE FROM FlightRoute" +
                  " WHERE RouteID=@RouteID", conn);
            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from the respective property of “project” object.
            cmd.Parameters.AddWithValue("@RouteID", flightRoute.RouteID);

            //Open a database connection.
            conn.Open();

            int Route = cmd.ExecuteNonQuery();
            //Close the database connection.
            conn.Close();
            return Route;
        }

        public int AddStaff(Staff staff)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = new SqlCommand
             (@"INSERT INTO Staff (StaffName, Gender, DateEmployed, Vocation, EmailAddr, Password, Status) OUTPUT INSERTED.StaffID 
             VALUES(@StaffName, @Gender, @DateEmployed, @Vocation, @EmailAddr, @Password, @Status)", conn);
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@Staffname", staff.StaffName);
            cmd.Parameters.AddWithValue("@Gender", staff.Gender);
            cmd.Parameters.AddWithValue("@DateEmployed", staff.DateEmployed);
            cmd.Parameters.AddWithValue("@Vocation", staff.Vocation);
            cmd.Parameters.AddWithValue("@EmailAddr", staff.EmailAddr);
            cmd.Parameters.AddWithValue("@Password", staff.Password);
            cmd.Parameters.AddWithValue("@status", staff.Status);

            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            staff.StaffID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return staff.StaffID;
        }
        public List<StaffViewModel> getAllStaffWithRoles()
        {
            // int x = 5; 
            // x 
            // list<objectclass> nameOfLiwst = new List<objectClass>();
            // list<Student> studentList = new List<Student>(); --> studentList 
            List<StaffViewModel> result = new List<StaffViewModel>();
            // 1) get a list of staffs - select frm staff
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @" SELECT StaffID, StaffName, Gender, Vocation FROM Staff";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            List<Staff> staffList = new List<Staff>();
            while (reader.Read())
            {
                // list.add(*) --> is to add the object into the list
                staffList.Add(
                    // this is to create new staff object 
                    new Staff
                    {
                        StaffID = reader.GetInt32(0),
                        StaffName = reader.GetString(1),
                        Gender = reader.GetString(2)[0],
                        Vocation = reader.GetString(3),
                    }
                );
            }

            reader.Close();
            //Close database connection
            conn.Close();


            //here i ahve a complete list of staff 
            // 2) for each staff gets all their roles --> staffView model objects put it inside the list 
            // select Role from FlightCrew WHERE StaffID = @selectedStaffID, 
            // we need the staffId to be dynamic 
            // cmd paramenters addwithvalue 
            //loop to get the staff out -if else or while
            // for ( declare counter ; condition to run for loop ; increase or decrease of counter )
            // foreach --> for ( object in list) 




            foreach (Staff s in staffList)
            {
                conn.Open();
                // this is the code to change selectedWStaffId into a value (staff.id) 
                cmd = conn.CreateCommand();
                cmd.CommandText = @"SELECT ScheduleID,Role from FlightCrew WHERE StaffID= @selectedStaffID";
                cmd.Parameters.AddWithValue("@selectedStaffID", s.StaffID);

                reader = cmd.ExecuteReader();
                //reader.hasRows will tell me if there is any result from the exectution of the query
                while (reader.Read())
                {
                    // this means reader has results 
                    // i need to create new staffViewModels objects then add it inside the list 
                    // new (object className) = { ... putting vlaues for the fields }
                    if (reader.HasRows)
                    {
                        result.Add(
                        new StaffViewModel
                        {
                            StaffID = s.StaffID,
                            StaffName = s.StaffName,
                            Gender = s.Gender,
                            Vocation = s.Vocation,
                            Role = reader.GetString(1),
                            ScheduleID = reader.GetInt32(0) //the number depends on which order of item come first
                        });
                    }
                }
                reader.Close();
                conn.Close();
            }

            // 3) you return. 
            //Close DataReader

            //Close database connection


            return result;
        }

        public List<FlightScheduleViewModel> getAvaliableFlightSchedules()
        {
            // setting up the command 
            // is to create a command object 
            SqlCommand cmd = conn.CreateCommand();
            // this is to put the query into the command object. 
            cmd.CommandText = @" SELECT * FROM FlightSchedule WHERE status != 'Cancelled'";

            conn.Open();

            // cmd.ExcuteReader() --> excutes the query
            // the result is then stored in reader
            // = assignment --> right to left.  
            SqlDataReader reader = cmd.ExecuteReader();

            //Create a new empty list that contains FlightScheduleViewModel
            List<FlightScheduleViewModel> flightSchedulesList = new List<FlightScheduleViewModel>();
            // int x; --> x is the name 
            // List<Student> StudentList = new List<Student>(); --> studentList; 

            //i would need to read from ther reader
            // i need to check if ther reader has any rows.  // proceed on to reading 
            while (reader.Read())
            {
                if (reader.HasRows)
                {

                    //add to list
                    //make new object
                    flightSchedulesList.Add(
                            new FlightScheduleViewModel
                            {
                                ScheduleId = reader.GetInt32(0),
                                FlightNumber = reader.GetString(1),
                                RouteId = reader.GetInt32(2),
                                AircraftId = reader.GetInt32(3),
                                DepartureDateTime = reader.GetDateTime(4),
                                ArrivalDateTime = reader.GetDateTime(5),
                                EconomyClassPrice = Convert.ToInt32(reader.GetDecimal(6)),
                                BusinessClassPrice = Convert.ToInt32(reader.GetDecimal(7)),
                                Status = reader.GetString(8)


                            }
                        );

                }

            }
            conn.Close();
            return flightSchedulesList;
        }
        public List<Staff> GetAvaliablePilotStaff()
        {
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"SELECT * FROM Staff WHERE Vocation= 'Pilot' AND Status ='Active'";

            conn.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            //This is hold my result to return 
            List<Staff> staffList = new List<Staff>();

            //while loop for the reader to read if there is something to read
            while (reader.Read())
            {
                //This is to check if the reader still has a row
                if (reader.HasRows)
                {
                    // i will be to extract the information and create a new staff
                    // i need to add into my staffList 

                    staffList.Add(
                        new Staff
                        {

                            StaffID = reader.GetInt32(0),
                            StaffName = reader.GetString(1),
                            Gender = reader.GetString(2)[0],
                            DateEmployed = reader.GetDateTime(3),
                            Vocation = reader.GetString(4),
                            EmailAddr = reader.GetString(5),
                            Password = reader.GetString(6),
                            Status = reader.GetString(7),

                        }


                        );

                }
            }

            return staffList;
        }



        public bool IsEmailExist(string email, int staffID)
        {
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a staff record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT StaffID FROM Staff
 WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and excute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != staffID)
                        //The email address is used by another staff
                        emailFound = true;
                }
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            return emailFound;
        }


        public bool IsPasswordCorrect(string email, string password)
        {
            bool pwcorrect = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get password with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT Password FROM Staff
                                WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and excute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetString(0) == password)

                        pwcorrect = true;
                }
            }
            else
            {
                pwcorrect = false;
            }
            reader.Close();
            conn.Close();

            return pwcorrect;
        }

        public void UpdateStatus(string id)
        {
            string status = "";
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT Status FROM Aircraft
                                WHERE AircraftID = @id;
                                ";
            cmd.Parameters.AddWithValue("@id", id);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows){
                while (reader.Read())
                {
                    status = reader.GetString(0);
                }
            }
            reader.Close();

            SqlCommand cmd1 = conn.CreateCommand();
            if (status == "Under Maintenance")
            {
                status = "Operational";
                cmd1.CommandText = @"UPDATE Aircraft
                                SET Status = @status
                                WHERE AircraftID = @id;
                                ";
                cmd1.Parameters.AddWithValue("@id", id);
                cmd1.Parameters.AddWithValue("@status", status);
            }
            else if (status == "Operational")
            {
                status = "Under Maintenance";
                String day = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                cmd1.CommandText = @"UPDATE Aircraft
                                SET Status = @status, DateLastMaintenance = @date
                                WHERE AircraftID = @id;
                                ";
                cmd1.Parameters.AddWithValue("@id", id);
                cmd1.Parameters.AddWithValue("@status", status);
                cmd1.Parameters.AddWithValue("@date", day);
            }
            
            SqlDataReader reader1 = cmd1.ExecuteReader();
            reader1.Close();
            conn.Close();
        }



        /* public bool checkAssignment(string role, int ScheduleID)
         {
             bool pilotAvail = false;
             SqlCommand cmd = conn.CreateCommand();

             cmd.CommandText = @"Select *from FlightCrew where role = 'Flight Captain' and ScheduleID = '@selectedScheduleID'";

             conn.Open();

             SqlDataReader reader = cmd.ExecuteReader();
             if (reader.HasRows)
             {
                 while (reader.Read())
                 {

                         if (reader.GetString(0) == "Flight Captain" || reader.GetString(0) == "Second Pilot")
                         {

                             return true;

                         }

                     else
                     {
                         return false;
                     }
                 }

             }
             reader.Close();
             conn.Close();

             return 
         } */
    }

}
