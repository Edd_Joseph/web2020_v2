﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using WebApplication1.Models;

namespace WebApplication1.DAL
{
    public class BookingDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public BookingDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "AirFlightsConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public int Add(Booking booking, int customerid, int scheduleid, decimal price)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CustomerID after insertion
            cmd.CommandText = @"INSERT INTO Booking (CustomerID, ScheduleID, PassengerName, PassportNumber, Nationality, SeatClass, AmtPayable,
                                Remarks)
                                OUTPUT INSERTED.CustomerID
                                VALUES(@customerid, @scheduleid, @name, @number, @country, @class, @amt,
                                @remarks)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.

            cmd.Parameters.AddWithValue("@customerid", customerid);
            cmd.Parameters.AddWithValue("@scheduleid", scheduleid);
            cmd.Parameters.AddWithValue("@name", booking.PassengerName);
            cmd.Parameters.AddWithValue("@number", booking.PassortNumber);
            cmd.Parameters.AddWithValue("@country", booking.Nationality);
            cmd.Parameters.AddWithValue("@class", booking.SeatClass);
            cmd.Parameters.AddWithValue("@amt", price);
            cmd.Parameters.AddWithValue("@remarks", booking.Remarks);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CustomerID after executing the INSERT SQL statement
            booking.BookingID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return booking.BookingID;
 
        }

        public List<Booking> GetBookingDetailsByCustomerId(int id)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM Booking WHERE CustomerID = @id";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “branchNo”.
            cmd.Parameters.AddWithValue("@id", id);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<Booking> booking = new List<Booking>();
            while (reader.Read())
            {
                booking.Add(
                new Booking
                {
                    BookingID = reader.GetInt32(0), //0: 1st column
                    CustomerID = reader.GetInt32(1),
                  
                    ScheduleID = reader.GetInt32(2),
                    PassengerName = reader.GetString(3),
                    PassortNumber = reader.GetString(4),
                    Nationality = reader.GetString(5),

                    SeatClass = reader.GetString(6),
                    AmtPayable = reader.GetDecimal(7),
                    Remarks = !reader.IsDBNull(8) ?
                                reader.GetString(8) : null,
                    DateTimeCreated = reader.GetDateTime(9),

                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return booking;
        }

        public List<Booking> GetBookingDetailsByBookingId(int id)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM Booking WHERE BookingID = @id";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “branchNo”.
            cmd.Parameters.AddWithValue("@id", id);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<Booking> booking = new List<Booking>();
            while (reader.Read())
            {
                booking.Add(
                new Booking
                {
                    BookingID = reader.GetInt32(0), //0: 1st column
                    CustomerID = reader.GetInt32(1),

                    ScheduleID = reader.GetInt32(2),
                    PassengerName = reader.GetString(3),
                    PassortNumber = reader.GetString(4),
                    Nationality = reader.GetString(5),

                    SeatClass = reader.GetString(6),
                    AmtPayable = reader.GetDecimal(7),
                    Remarks = !reader.IsDBNull(8) ?
                                reader.GetString(8) : null,
                    DateTimeCreated = reader.GetDateTime(9),

                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return booking;
        }

        public List<FlightScheduleViewModel> GetFlightScheduleByStatus(string status)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE Status = @status";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “branchNo”.
            cmd.Parameters.AddWithValue("@status", status);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<FlightScheduleViewModel> schedulelist = new List<FlightScheduleViewModel>();
            while (reader.Read())
            {
                schedulelist.Add(
                new FlightScheduleViewModel
                {
                    ScheduleId = reader.GetInt32(0), //0: 1st column
                     //1: 2nd column
                    RouteId = reader.GetInt32(2),
                    AircraftId = !reader.IsDBNull(3) ?
                                reader.GetInt32(3) : (int?)null,
                    FlightNumber = reader.GetString(1),
                    DepartureDateTime = !reader.IsDBNull(4) ?
                                reader.GetDateTime(4) : (DateTime?)null,
                    ArrivalDateTime = !reader.IsDBNull(5) ?
                                reader.GetDateTime(5) : (DateTime?)null, 
                    EconomyClassPrice = reader.GetDecimal(6), 
                    BusinessClassPrice = reader.GetDecimal(7), 
                    Status = reader.GetString(8), 
   
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return schedulelist;
        }

        public List<FlightScheduleViewModel> GetFlightScheduleByScheduleId(int id)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all branches
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE ScheduleID = @scheduleid";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “branchNo”.
            cmd.Parameters.AddWithValue("@scheduleid", id);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<FlightScheduleViewModel> schedulelist = new List<FlightScheduleViewModel>();
            while (reader.Read())
            {
                schedulelist.Add(
                new FlightScheduleViewModel
                {
                    ScheduleId = reader.GetInt32(0), //0: 1st column
                                                     //1: 2nd column
                    RouteId = reader.GetInt32(2),
                    AircraftId = !reader.IsDBNull(3) ?
                                reader.GetInt32(3) : (int?)null,
                    FlightNumber = reader.GetString(1),
                    DepartureDateTime = !reader.IsDBNull(4) ?
                                reader.GetDateTime(4) : (DateTime?)null,
                    ArrivalDateTime = !reader.IsDBNull(5) ?
                                reader.GetDateTime(5) : (DateTime?)null,
                    EconomyClassPrice = reader.GetDecimal(6),
                    BusinessClassPrice = reader.GetDecimal(7),
                    Status = reader.GetString(8),

                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return schedulelist;
        }

        public int GetScheduleId(int id)
        {

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT ScheduleID FROM Booking
                              WHERE BookingID = @bookingid";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@bookingid", id);
            //Open a database connection
            int value = 0;
            conn.Open();
            //Execute the SELECT SQL through a Data


            
            value = (int)cmd.ExecuteScalar();

            
            //Close the database connection
            conn.Close();
            return value;
        }

        public List<Decimal> GetPrice(int id)
        {
            
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT EconomyClassPrice, BusinessClassPrice FROM FlightSchedule
                              WHERE ScheduleID = @scheduleid";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@scheduleid", id);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a Data
            SqlDataReader reader = cmd.ExecuteReader();

            List<decimal> pricelist = new List<decimal>();
            while (reader.Read())
            {
                pricelist.Add(reader.GetDecimal(0));
                pricelist.Add(reader.GetDecimal(1));
            }


            reader.Close();
            //Close the database connection
            conn.Close();
            return pricelist;
        }
        
    }
}
