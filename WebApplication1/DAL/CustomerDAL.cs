﻿using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.IO;
using WebApplication1.Models;


namespace WebApplication1.DAL
{
    public class CustomerDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public CustomerDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "AirFlightsConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public int Add(Customer customer)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CustomerID after insertion
            cmd.CommandText = @"INSERT INTO Customer (CustomerName, Nationality, BirthDate, TelNo,
                                EmailAddr)
                                OUTPUT INSERTED.CustomerID
                                VALUES(@name, @country, @dob, @telno,
                                @email)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", customer.CustomerName);
            cmd.Parameters.AddWithValue("@country", customer.Nationality);
            cmd.Parameters.AddWithValue("@dob", customer.BirthDate);
            cmd.Parameters.AddWithValue("@telno", customer.TelNo);
            cmd.Parameters.AddWithValue("@email", customer.EmailAddr);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CustomerID after executing the INSERT SQL statement
            customer.CustomerID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return customer.CustomerID;
        }

        public bool IsEmailExist(string email, int customerId)
        {
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a customer record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT CustomerID FROM Customer
                                WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and excute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != customerId)
                        //The email address is used by another staff
                        emailFound = true;
                }
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            return emailFound;
        }

        public bool IsPasswordCorrect(string email, string password)
        {
            bool pwcorrect = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get password with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT Password FROM Customer
                                WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and excute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetString(0) == password)

                        pwcorrect = true;
                }
            }
            else
            {
                pwcorrect = false;
            }
            reader.Close();
            conn.Close();

            return pwcorrect;
        }

        public string GetName(string email)
        {
            string name = "";
            //Create a SqlCommand object and specify the SQL statement
            //to get a customer record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT CustomerName FROM Customer
                                WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and excute the SQL statement
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated

            name = (string)cmd.ExecuteScalar();


            conn.Close();
            return name;
        }

        // Return number of row updated
        public int UpdatePassword(int id, string password)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE Customer SET Password=@password
                                WHERE CustomerID=@selectedCustomerID";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@selectedCustomerID", id);
            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE

            int count = 0;
            count += cmd.ExecuteNonQuery();

            //Close the database connection
            conn.Close();
            return count;
        }

        public int GetId(string email)
        {
            int id = -1;
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT CustomerID FROM Customer
                              WHERE EmailAddr = @selectedEmail";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader



            id = (int)cmd.ExecuteScalar();



            //Close the database connection
            conn.Close();
            return id;
        }

    }
}
